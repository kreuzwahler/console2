﻿using System;

namespace Iff
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 100;

            if (a % 2 == 0)
                Console.WriteLine("even!");
            else Console.WriteLine("odd!");

        }            
    }
}
